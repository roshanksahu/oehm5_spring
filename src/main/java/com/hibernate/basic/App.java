package com.hibernate.basic;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.hibernate.basic.dao.AssociationsDAO;
import com.hibernate.basic.dao.StudentDAO;
import com.hibernate.basic.dao.StudentHqlDAO;
import com.hibernate.basic.dto.Actor;
import com.hibernate.basic.dto.Brand;
import com.hibernate.basic.dto.Captain;
import com.hibernate.basic.dto.Country;
import com.hibernate.basic.dto.Model;
import com.hibernate.basic.dto.Movie;
import com.hibernate.basic.dto.State;
import com.hibernate.basic.dto.Student;
import com.hibernate.basic.dto.Team;

public class App {
	public static void main(String[] args) {
		/*Student student = new Student();
		// student.setId(102L);
		student.setName("Survi");
		student.setBranch("MCA");
		student.setAge(24L);
		student.setContactNumber(4567397L);

		StudentDAO studentDAO = new StudentDAO();
		studentDAO.saveStudentDetails(student);*/

		// studentDAO.updateStudentById(100L, 5675936L);

		// Student std = studentDAO.getStudentById(101L);
		// System.out.println(std);

		// Student std1 = studentDAO.getStudentById(101L);
		// System.out.println(std1);

		// studentDAO.saveStudentDetails(student);

		/*StudentHqlDAO studentHqlDAO = new StudentHqlDAO();

		studentHqlDAO.getStudents().forEach(a -> {
			System.out.println(a);
		});
		System.out.println("---------------------------------------------------------------------");
		studentHqlDAO.getStudents().forEach(a -> {
			System.out.println(a);
		});*/

		// Student std = studentHqlDAO.getStudentByContactNumber(4567399L);
		// System.out.println(std);

		// studentHqlDAO.updateContactNumberById(100L, 8951606106L);

		// studentHqlDAO.deleteById(102L);
		
		
		/*------------------------------------------------------------------------------------------*/
		
		Captain captain = new Captain();
		captain.setName("Virat Kohli");
		captain.setAge(32L);
		captain.setJersyNumber("7");
		captain.setToatalMatches(300L);
		
		Team team = new Team();
		team.setCountryName("India");
		team.setRank("1");
		team.setSize(11L);
		team.setTeamType("cricket");
		
		captain.setTeam(team);
		
		AssociationsDAO dao = new AssociationsDAO();
		dao.saveCaptainDetails(captain);
		
		
		/*------------------------------------------------------------------------------------------*/
		/*
		List<Model> list = new ArrayList<Model>();
		
		Model m1 = new Model();
		m1.setName("6T");
		m1.setColor("blue");
		m1.setModelNumber("ONEPLUS123");
		m1.setPrice(34000.00);
		
		Model m2 = new Model();
		m2.setName("8T");
		m2.setColor("black");
		m2.setModelNumber("ONEPLUS122");
		m2.setPrice(40000.00);
		
		list.add(m1);
		list.add(m2);
		
		Brand brand = new Brand();
		brand.setName("ONEPLUS");
		brand.setType("Mobile");
		brand.setYearOfEstablishment("2012");
		brand.setAmbasador("Robert");
		brand.setModels(list);
		
		AssociationsDAO dao = new AssociationsDAO();
		dao.saveBrandDetails(brand);*/
		
		/*------------------------------------------------------------------------------------------*/
		
		/*State state1 = new State();
		state1.setName("Odisha");
		state1.setCapitalName("bbsr");
		State state2 = new State();
		state2.setName("AP");
		state2.setCapitalName("HYD");
		State state3 = new State();
		state3.setName("GJ");
		state3.setCapitalName("GandhiN");
		
		Country country = new Country();
		country.setCountryName("IND");
		country.setNumberOfStates(30L);
		
		state1.setCountry(country);
		state2.setCountry(country);
		state3.setCountry(country);
		List<State> list = new ArrayList<>();
		list.add(state1);
		list.add(state2);
		list.add(state3);
		
		for (State state : list) {
			System.out.println(list);
		}
		AssociationsDAO dao = new AssociationsDAO();
		dao.saveStateDetails(country, list);*/
		
		/*------------------------------------------------------------------------------------------*/
		
		/*Actor actor = new Actor();
		actor.setName("srk");
		actor.setAge(54L);
		
		Actor actor2 = new Actor();
		actor2.setName("sk");
		actor2.setAge(55L);
		
		List<Actor> list = new ArrayList<>();
		list.add(actor);
		list.add(actor2);
		
		Movie movie = new Movie();
		movie.setName("kkhh");
		movie.setReleaseDate(new Date());
		movie.setRating("3");
		
		Movie movie2 = new Movie();
		movie2.setName("karan arjun");
		movie2.setRating("5");
		movie2.setReleaseDate(new Date());
		
		List<Movie> list1 = new ArrayList<>();
		list1.add(movie);
		list1.add(movie2);
		
		movie.setActors(list);
		movie2.setActors(list);
		
		AssociationsDAO dao = new AssociationsDAO();
		dao.saveMovieDetails(movie);
		dao.saveMovieDetails(movie2);*/
	}
}
