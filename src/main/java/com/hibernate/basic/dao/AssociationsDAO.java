package com.hibernate.basic.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.hibernate.basic.dto.Brand;
import com.hibernate.basic.dto.Captain;
import com.hibernate.basic.dto.Country;
import com.hibernate.basic.dto.Movie;
import com.hibernate.basic.dto.State;
import com.hibernate.basic.dto.Team;
import com.hibernate.basic.util.SessionFactoryUtil;

public class AssociationsDAO {
	
	public void saveTeamDetails(Team team)
	{
		Session session = SessionFactoryUtil.getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		session.save(team);
		tx.commit();
	}
	
	public void saveCaptainDetails(Captain captain)
	{
		Session session = SessionFactoryUtil.getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		session.save(captain);
		tx.commit();
	}
	public void saveBrandDetails(Brand brand)
	{
		Session session = SessionFactoryUtil.getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		session.save(brand);
		tx.commit();
	}
	public void saveMovieDetails(Movie movie)
	{
		Session session = SessionFactoryUtil.getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		session.save(movie);
		tx.commit();
	}
	public void saveStateDetails(Country country,List<State> list)
	{
		Session session = SessionFactoryUtil.getSessionFactory().openSession();
		Transaction tx = session.beginTransaction();
		session.save(country);
		for (State state : list) {
			session.save(state);
		}
		tx.commit();
	}
}
