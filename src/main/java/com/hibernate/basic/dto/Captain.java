package com.hibernate.basic.dto;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "captain")
public class Captain implements Serializable{
	@Id
	@Column(name = "id")
	@GenericGenerator(name = "cap_auto", strategy = "increment")
	@GeneratedValue(generator = "cap_auto")
	private Long id;

	@Column(name = "name")
	private String name;
	
	@Column(name = "age")
	private Long age;
	
	@Column(name = "jersy_num")
	private String jersyNumber;
	
	@Column(name = "tot_matches")
	private Long toatalMatches;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "t_id")
	private Team team;

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getAge() {
		return age;
	}

	public void setAge(Long age) {
		this.age = age;
	}

	public String getJersyNumber() {
		return jersyNumber;
	}

	public void setJersyNumber(String jersyNumber) {
		this.jersyNumber = jersyNumber;
	}

	public Long getToatalMatches() {
		return toatalMatches;
	}

	public void setToatalMatches(Long toatalMatches) {
		this.toatalMatches = toatalMatches;
	}
	
	public Captain() {
		super();
	}
}
