package com.hibernate.basic.dto;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "brand")
public class Brand implements Serializable{
	@Id
	@Column(name = "id")
	@GenericGenerator(name = "cap_auto", strategy = "increment")
	@GeneratedValue(generator = "cap_auto")
	private Long id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "type")
	private String type;
	
	@Column(name = "yoe")
	private String yearOfEstablishment;
	
	@Column(name = "ambasador")
	private String ambasador;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "b_id")
	private List<Model> models;

	public Brand() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getYearOfEstablishment() {
		return yearOfEstablishment;
	}

	public void setYearOfEstablishment(String yearOfEstablishment) {
		this.yearOfEstablishment = yearOfEstablishment;
	}

	public String getAmbasador() {
		return ambasador;
	}

	public void setAmbasador(String ambasador) {
		this.ambasador = ambasador;
	}

	public List<Model> getModels() {
		return models;
	}

	public void setModels(List<Model> models) {
		this.models = models;
	}

}
