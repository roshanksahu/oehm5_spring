package com.hibernate.basic.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "model")
public class Model implements Serializable{
	@Id
	@Column(name = "id")
	@GenericGenerator(name = "cap_auto", strategy = "increment")
	@GeneratedValue(generator = "cap_auto")
	private Long id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "model_number")
	private String modelNumber;
	
	@Column(name = "price")
	private double price;
	
	@Column(name = "color")
	private String color;

	public Model() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getModelNumber() {
		return modelNumber;
	}

	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	
	
}
