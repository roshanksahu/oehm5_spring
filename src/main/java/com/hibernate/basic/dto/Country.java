package com.hibernate.basic.dto;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "country")
public class Country implements Serializable{
	@Id
	@Column(name = "id")
	@GenericGenerator(name = "country_auto" , strategy = "increment")
	@GeneratedValue(generator = "country_auto")
	private Long id;
	
	@Column(name = "country_name")
	private String countryName;
	
	@Column(name = "no_of_states")
	private Long numberOfStates;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public Long getNumberOfStates() {
		return numberOfStates;
	}

	public void setNumberOfStates(Long numberOfStates) {
		this.numberOfStates = numberOfStates;
	}

	@Override
	public String toString() {
		return "Country [id=" + id + ", countryName=" + countryName + ", numberOfStates=" + numberOfStates + "]";
	}
	
	
}
