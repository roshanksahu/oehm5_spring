package com.hibernate.basic.dto;

import java.io.Serializable;
import java.util.*;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "movie")
public class Movie implements Serializable{
	@Id
	@Column(name = "id")
	@GenericGenerator(name = "movie_auto", strategy = "increment")
	@GeneratedValue(generator = "movie_auto")
	private Long id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "release_date")
	private Date releaseDate;
	
	@Column(name = "rating")
	private String rating;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "m_a_common" , 
	joinColumns = { @JoinColumn(name = "m_id")} , 
	inverseJoinColumns = { @JoinColumn(name = "a_id")})
	private List<Actor> actors;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public List<Actor> getActors() {
		return actors;
	}

	public void setActors(List<Actor> actors) {
		this.actors = actors;
	}
}
