package com.hibernate.basic.util;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

public class MyCustomGenarator implements IdentifierGenerator{

	@Override
	public Serializable generate(SharedSessionContractImplementor contractImplementor, Object object) throws HibernateException {
		SessionImplementor implementor = (SessionImplementor) contractImplementor;
		
		Connection con = implementor.connection();
		try 
		{
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery("select max(id) from student_details");
			if(rs.next())
			{
				Long id = rs.getLong(0);
				return id+1;
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return null;
	}

}

